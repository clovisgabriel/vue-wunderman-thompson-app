import organizations from '../assets/jsonStore/organizations.json';
 
const OrganizationAPI = {
    async getOrganizations() {
        return new Promise((resolve, reject) => {
            if (organizations) {
                resolve(organizations);
            }
            reject();
        })
    },
    async filterOrganizations(pagination, text) {
        return new Promise((resolve, reject) => {
            let { page, size } = pagination;
            const start = (page - 1) * size;
            const end = start + size;
            let selectedOrganizations = organizations.filter(i => i.name.includes(text));
            
            if (selectedOrganizations.length > 0) {
                const organizationsList = selectedOrganizations.slice(start, end);
                pagination.totalElements = selectedOrganizations.length;
    
                const content = {
                    data: organizationsList,
                    pagination
                };
                resolve(content);
            }
            reject();
        });
    },
    async getOrganizationById(id) {
        return new Promise((resolve, reject) => {
            const organization = organizations.find(item => item._id === id);
            if (organization) {
                resolve(organization);
            }
            reject();
        })
    }
}
 
export default OrganizationAPI;
 

