import tickets from '../assets/jsonStore/tickets.json';
 
const TicketAPI = {
    async getTickets(id) {
        return new Promise((resolve, reject) => {
            const ticket = tickets.find(item => item.assignee_id === id);
            if (ticket) {
                resolve(ticket);
            }
            reject();
        })
    },
    async filterTickets(pagination, text) {
        return new Promise((resolve, reject) => {
            let { page, size } = pagination;
            const start = (page - 1) * size;
            const end = start + size;
            let selectedTickets = tickets.filter(i => i.subject.includes(text));
            
            const ticketsList = selectedTickets.slice(start, end).map(item => {
                return {
                    ...item,
                    ticket: item.subject || '-'
                }
            });
            pagination.totalElements = selectedTickets.length;

            const content = {
                data: ticketsList,
                pagination
            };
            if (content) {
                resolve(content);
            }
            reject();
        });
    }
}
 
export default TicketAPI;
 

