import users from '../assets/jsonStore/users.json';
import organizations from '../assets/jsonStore/organizations.json';
import tickets from '../assets/jsonStore/tickets.json';
import moment from 'moment';

const UserAPI = {
    async getUsers(pagination) {
        return new Promise((resolve, reject) => {
            let { page, size } = pagination;
            const start = (page - 1) * size;
            const end = start + size;
            const usersList = users.slice(start, end);
            pagination.totalElements = users.length;

            const content = {
                users: usersList,
                pagination
            }
            if (content) {
                resolve(content);
            }
            reject();
        });
    },
    async filterUsers(pagination, text, filterBy) {
        return new Promise((resolve, reject) => {
            let { page, size } = pagination;
            const start = (page - 1) * size;
            const end = start + size;
            let selectedUsers = [];
            let selectedOrganization = null;    
            let selectedTicket = null;    

            switch (filterBy) {
                case 'name':
                    selectedUsers = users.filter(item => item.name.includes(text));
                    selectedUsers = selectedUsers.map(item => {
                        const organization = organizations.find(i => i._id === item.organization_id);
                        const ticket = tickets.find(i => i.assignee_id === item._id);
                        return {
                            ...item,
                            organization_name: organization ? organization.name : '',
                            ticket: ticket ? ticket.subject + ' - ' + ticket.description : ''
                        }
                    });
                    break;
                case 'alias':
                    selectedUsers = users.filter(item => {
                        if (item.alias) {
                            return item.alias.includes(text);
                        }
                    });
                    break;
                case 'created_at':
                    selectedUsers = users.filter(item => {
                        const date = moment.utc(item.created_at.split(' ').join('')).local().format('YYYY-MM-DD');
                        const receivedDate = moment(text).format('YYYY-MM-DD');
                        return moment(date).isSameOrAfter(moment(receivedDate));
                    });
                    break;
                case 'email':
                    selectedUsers = users.filter(item => {
                        if (item.email) {
                            return item.email.includes(text);
                        }
                    });
                    break;
                case 'phone':
                    selectedUsers = users.filter(item => {
                        if (item.phone) {
                            return item.phone.includes(text);
                        }
                    });
                    break;
                case 'active':
                    selectedUsers = users.filter(item => {
                        if (item.active) {
                            return item.active.includes(text);
                        }
                    });
                    break;
                case 'verified':
                    selectedUsers = users.filter(item => {
                        if (item.verified) {
                            return item.verified.includes(text);
                        }
                    });
                    break;
                case 'suspended':
                    selectedUsers = users.filter(item => {
                        if (item.suspended) {
                            return item.suspended.includes(text);
                        }
                    });
                    break;
                case 'shared':
                    selectedUsers = users.filter(item => {
                        if (item.shared) {
                            return item.shared.includes(text);
                        }
                    });
                    break;
                case 'organization':
                    selectedOrganization = organizations.find(i => i.name.includes(text));
                    selectedUsers = users.filter(item => {
                        if (selectedOrganization) {
                            return item.organization_id === selectedOrganization._id;
                        }
                    });
                    break;
                case 'ticket':
                    selectedTicket = tickets.find(i => i.subject.includes(text));
                    selectedUsers = users.filter(item => {
                        if (selectedTicket) {
                            return item._id === selectedTicket.assignee_id;
                        }
                    });
                    break;
                case 'role':
                    selectedUsers = users.filter(item => {
                        if (item.role) {
                            return item.role.includes(text);
                        }
                    });
                    break;
                case 'tag':
                    selectedUsers = users.filter(item => {
                        if (item.tag) {
                            return item.tag.includes(text);
                        }
                    });
                    break;
                default:
                    break;
            }
            const usersList = selectedUsers.slice(start, end);
            pagination.totalElements = selectedUsers.length;

            const content = {
                data: usersList,
                pagination
            };
            
            if (content) {
                resolve(content);
            }
            reject();
        });
    },
    async filterUsersByBoolean(pagination, bool, filterBy) {
        return new Promise((resolve, reject) => {
            let { page, size } = pagination;
            const start = (page - 1) * size;
            const end = start + size;
            let selectedUsers = [];
            selectedUsers = users.filter(item => {
                if (item.alias) {
                    return item[filterBy] === bool;
                }
            });

            const usersList = selectedUsers.slice(start, end);
            pagination.totalElements = selectedUsers.length;

            const content = {
                data: usersList,
                pagination
            };
            resolve(content);
            
            if (content) {
                reject();
            }
        });
    },
    async saveUser(user) {
        return new Promise((resolve, reject) => {
            if (user) {
                const lastIndex = users.length + 1;
                user._id = lastIndex;
                user.created_at = moment().format('YYYY-MM-DDTHH:mm:ssZ');
                users.unshift(user);
                resolve(true);
            }
            reject();
        });
    },
    async getUserById(id) {
        return new Promise((resolve, reject) => {
            if (id > 0) {
                const user = users.find(item => item._id === id);
                if (user.organization_id) {
                    const organization = organizations.find(item => item._id === user.organization_id)
                    user.organization_name = organization.name;
                } else {
                    user.organization_name = '';
                }

                const userTickets = tickets.filter(item => item.organization_id === user.organization_id);
                if (userTickets) {
                    user.tickets = userTickets;
                } else {
                    user.tickets = '';
                }

                if (user) {
                    resolve(user);
                }
                reject('User not found!');
            }
            reject('Error retrieving user!');
        });
    },
    async deleteUser(userId) {
        return new Promise((resolve, reject) => {
            if (userId > 0) {
                const index = users.findIndex(item => item._id === userId);
                users.splice(index, 1);
                resolve(true);
            }
            reject('Cannot delete user');
        });
    }
}
 
export default UserAPI;
 

