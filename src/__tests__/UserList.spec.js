
import { shallowMount } from '@vue/test-utils'
import UserList from '../components/UserList';
import usersMock from './files/users-mock.json';
import organizationsMock from './files/organizations-mock.json';
import moment from 'moment';

describe('UserList.vue test', () => {
  let wrapper = null

  beforeEach(() => {
    wrapper = shallowMount(UserList);
  })
  afterEach(() => {
    wrapper.destroy()
  })

  it('has the users list', () => {
    wrapper.setData({ users: usersMock });
    expect(wrapper.vm.users).toEqual(expect.arrayContaining(usersMock));
  })
  
  it('searching by name Eliana Gouth', () => {
    wrapper.setData({ users: usersMock });
    const filterUsers = jest.fn((users, name) => {
      const elements = users.filter(item => item.name.includes(name));
      return elements;
    });

    expect(filterUsers(wrapper.vm.users, 'Eliana Gouth')).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: 'Eliana Gouth'
        })
      ])
    );
  });

  it('searching by alias Miss Coffey', () => {
    wrapper.setData({ users: usersMock });
    const filterUsers = jest.fn((users, alias) => {
      const elements = users.filter(item => item.alias.includes(alias));
      return elements;
    });
    
    expect(filterUsers(wrapper.vm.users, 'Miss Coffey')).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          alias: 'Miss Coffey'
        })
      ])
    );
  });
  
  it('searching by date same or after 01/01/2019 for attribute created_at', () => {
    wrapper.setData({ users: usersMock });
    const filterUsers = jest.fn((users, date_created_at) => {
      const elements = users.filter(item => {
        const date = moment.utc(item.created_at.split(' ').join('')).local().format('YYYY-MM-DD');
        const receivedDate = moment(date_created_at).format('YYYY-MM-DD');
        return moment(date).isSameOrAfter(moment(receivedDate));
      });
      return elements;
    });
    
    expect(filterUsers(wrapper.vm.users, '2010-01-01T00:29:25').length)
      .toBe(3);
    expect(filterUsers(wrapper.vm.users, '2019-01-01T00:29:25').length)
      .toBe(2);
    expect(filterUsers(wrapper.vm.users, '2022-01-01T00:29:25').length)
      .toBe(0);
  });

  it('searching by organization', () => {
    wrapper.setData({ users: usersMock });
    const filterUsers = jest.fn((users, organization) => {
      const selectedOrganization = organizationsMock.find(i => i.name.includes(organization));
      const selectedUsers = users.filter(item => {
          if (selectedOrganization) {
              return item.organization_id === selectedOrganization._id;
          }
      });
      return selectedUsers;
    });
    
    expect(filterUsers(wrapper.vm.users, 'Plasmos').length)
      .toBe(1);
    expect(filterUsers(wrapper.vm.users, 'Plasmos 2').length)
      .toBe(0);
  });
  
  it('searching by active', () => {
    wrapper.setData({ users: usersMock });
    const filterUsers = jest.fn((users, active) => {
      const selectedUsers = users.filter(i => i.active === active);
      return selectedUsers;
    });
    
    expect(filterUsers(wrapper.vm.users, true).length)
      .toBe(2);
    expect(filterUsers(wrapper.vm.users, false).length)
      .toBe(1);
  });
  
  it('deleting user', () => {
    const userId = 1;

    const deleteUser = jest.fn((userId) => {
      if (userId > 0) {
        const index = users.findIndex(item => item._id === userId);
        usersMock.splice(index, 1)
      }
    });

    expect(usersMock.length).toBe(3);
    deleteUser(userId);
    expect(usersMock.length).toBe(2);
    expect(usersMock).not.toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: 'Eliana Gouth'
        })
      ])
    );
  })
}); 