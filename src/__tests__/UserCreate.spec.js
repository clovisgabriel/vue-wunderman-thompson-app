
import { shallowMount } from '@vue/test-utils'
import UserCreate from '../components/UserCreate';
import usersMock from './files/users-mock.json';
import moment from 'moment';

describe('UserCreate.vue test', () => {
  let wrapper = null

  beforeEach(() => {
    wrapper = shallowMount(UserCreate);
  })
  afterEach(() => {
    wrapper.destroy()
  })

  it('saving new user', () => {
    wrapper.setData({
      user: {
        name: 'Nilson Moth',
        alias: 'Frou Thirn',
        email: 'nilsonmoth@cotk.com',
        phone: '9365-482-943',
        organization_id: 103,
        role: 'admin'
      }
    });

    const saveUser = jest.fn((user) => {
      const lastIndex = usersMock.length + 1;
      user._id = lastIndex;
      user.created_at = moment().format('YYYY-MM-DDTHH:mm:ssZ');
      usersMock.unshift(user);
    });

    expect(usersMock.length).toBe(3);
    saveUser(wrapper.vm.user);
    expect(usersMock.length).toBe(4);
    expect(usersMock).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          name: 'Nilson Moth'
        })
      ])
    );
  })
}); 