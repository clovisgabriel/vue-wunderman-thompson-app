import Vue from 'vue';
import Router from 'vue-router';
import UserList from '../components/UserList.vue';
import UserCreate from '../components/UserCreate.vue';
import UserView from '../components/UserView.vue';

Vue.use(Router);

export default new Router({
  mode: "history",
  hash: false,
  routes: [
    {
      path: "/",
      name: "users",
      component: UserList
    },
    {
      path: "/create",
      name: "createUser",
      component: UserCreate
    },
    {
      path: "/view/:id",
      name: "viewUser",
      component: UserView
    }
  ]
});

