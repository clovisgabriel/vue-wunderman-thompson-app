# Wunderman Thompson Application

Basic search application.

**Follow the steps bellow to run the project.**

Firstly, open Visual Studio Code. In the top menu, click on **File -> Open folder...**. Select the folder of this project, already downloaded. Then, open the terminal clicking on **Terminal -> New Terminal** and type the commands bellow.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Unit Test with JEST
```
npm run test
```
Link to the videos showing the application:

- Insertion 
https://drive.google.com/file/d/1R3bpdn1i5RB_Cm81-gyyg8MrOcOQMrrQ/view?usp=sharing

- List, search and delete
https://drive.google.com/file/d/17XEsUbCwcdN4W3kH2yGVnoSdp9Ia1SI-/view?usp=sharing
